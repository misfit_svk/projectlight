﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PL.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/js/lib/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                        "~/js/lib/jquery.dataTables.min.js",
                        "~/js/lib/dataTables.bootstrap.min.js",
                        "~/js/lib/dataTables.colReorder.min.js",
                        "~/js/lib/materialize.min.js",
                        "~/js/lib/materialize.clockpicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/js/lib/jquery-ui.min.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(

                      "~/css/bootstrap.min.css",
                      "~/css/dataTables.bootstrap.min.css",
                      "~/css/colReorder.bootstrap.min.css",
                      "~/css/fonts-giigleleaspis.css",
                      "~/css/jquery.dataTables.min.css",
                      "~/css/jquery.dataTables_themeroller.min.css",
                      "~/css/jquery-ui.min.css",
                      "~/css/materialize.min.css", 
                      "~/css/Material-icon.css",
                      "~/css/materialize.clockpicker.css",
                      "~/css/Oxygen.css",
                      "~/css/Quicksand.css",
                      
                      "~/css/site.css"));

            bundles.Add(new StyleBundle("~/Content/main").Include(
                "~/css/main.css"));

            bundles.Add(new StyleBundle("~/Content/font").Include(
                "~/css/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/materialize").Include(
                "~/css/materialize.min.css",
                "~/css/Material-icon.css",
                "~/css/materialize.clockpicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                     "~/js/main.js"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/materialize").Include(
                     "~/js/lib/materialize.min.js",
                     "~/js/lib/materializeClockPicker.js"
                     ));

        }
    }
}