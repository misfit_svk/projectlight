﻿/**
 * Created by milan on 3/27/17.
 */


$( document ).ready(function() {
    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
    }
    );
    var checkTemp = true;
    $('#checkAll,#checkAll2').click(function () {
        $('.checkBox').attr('checked', checkTemp);
        checkTemp = !checkTemp;
    });

    $('select').material_select();

    $('.timepicker').pickatime({
        default: 'now',
        twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
        donetext: 'OK',
        autoclose: false,
        vibrate: true // vibrate the device when dragging clock hand
    });

});

