/**
 * Created by milan on 3/27/17.
 */
$( document ).ready(function() {


    $("#plusFont").click(function () {
        var fontSize = parseInt($("body").css("font-size"));
        if (fontSize >= 15 && fontSize < 23) {
            $(this).removeClass("oppLayer");
            $("#minusFont").removeClass("oppLayer");
            fontSize++;
            fontSize = fontSize + "px";
            $("body").css({'font-size': fontSize});
            fontSize = parseInt($("body").css("font-size"));
            if (fontSize == 23){
                $(this).addClass("oppLayer");
            }
        } else {
            $("body").css({'font-size': fontSize});
            if (fontSize == 23){
                $(this).addClass("oppLayer");
            }
        }
    });
    $("#minusFont").click(function () {
        var fontSize = parseInt($("body").css("font-size"));
        if (fontSize > 15 && fontSize <= 23) {
            $(this).removeClass("oppLayer");
            $("#plusFont").removeClass("oppLayer");
            fontSize--;
            fontSize = fontSize + "px";
            $("body").css({'font-size': fontSize});
            fontSize = parseInt($("body").css("font-size"));
            if (fontSize == 15){
                $(this).addClass("oppLayer");
            }
        } else {
            $("body").css({'font-size': fontSize});
            if (fontSize == 15){
                $(this).addClass("oppLayer");
            }
        }

    });


    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 300
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            draggable: true // Choose whether you can drag to open on touch screens
        }
    );
    var checkTemp = true;
    $('#checkAll,#checkAll2').click(function () {
        $('.checkBox').attr('checked', checkTemp);
        checkTemp = !checkTemp;
    });

    $('select').material_select();


    $("#closeClick").click(function(){
        var actualCloseStat = $(".closable").hasClass("displayNone");

        if (actualCloseStat == true){
            console.log(actualCloseStat);
            $(".closable").removeClass("displayNone");

        } else {
            console.log(actualCloseStat);
            $(".closable").addClass("displayNone");
        }

    });
/*
    $('.timepicker').pickatime({
        default: 'now',
        twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
        donetext: 'OK',
        autoclose: false,
        vibrate: true // vibrate the device when dragging clock hand
    });
*/


    //var effVal = $('#test55').val();
   // $('#rangeShow').append(effVal.toString()  + "%");
});
