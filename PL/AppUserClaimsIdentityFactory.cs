﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Threading.Tasks;
using PL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PL
{
    public class AppUserClaimsIdentityFactory : ClaimsIdentityFactory<AppUser>
    {
        public AppUserClaimsIdentityFactory()
        {
            UserIdClaimType = ClaimTypes.NameIdentifier;

        }


        public override async Task<ClaimsIdentity> CreateAsync(UserManager<AppUser> manager, AppUser user, string authenticationType)
        {
            var identity = await base.CreateAsync(manager, user, authenticationType);
            identity.AddClaim(new Claim(ClaimTypes.GivenName, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, user.privilages.ToString()));


            return identity;
        }
    }
}