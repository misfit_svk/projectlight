﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using PL.Models;
using PL.App_Start;
using PL.Models.Data;
using System.Security.Claims;

namespace PL.Controllers
{
    [AllowAnonymous]
    public class AuthenController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        AppDbContext db = new AppDbContext();
        public AuthenController()
            : this(Startup.UserManagerFactory.Invoke())
        {

        }
        public AuthenController(UserManager<AppUser> userManager) {
            this.userManager = userManager;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            var model = new LoginModel
            {
                ReturnUrl = returnUrl
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginModel model, string Btnvalue)
        {
            switch (Btnvalue)
            {
                case "Continue":
                    return RedirectToAction("index", "settings", new { filter = "all", sft = "N" });
                    break;

                case "Login":
                if (!ModelState.IsValid)
                {
                    return View();
                }
                var user = await userManager.FindAsync(model.UserName, model.Password);

                if (user != null)
                {
                    var identity = await userManager.CreateIdentityAsync(
                        user, DefaultAuthenticationTypes.ApplicationCookie);

                    GetAuthenticationManager().SignIn(identity);

                    return RedirectToAction("index", "settings", new { filter = "all", sft = "N" });
                }

                // user authN failed
                ModelState.AddModelError("", "Invalid user name or password");
                    break;
                default:
                    ModelState.AddModelError("", "Invalid action");
                    return View();
                    break;
            }
            return View();
        }

        //Redirect to log in if not signed
        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Login", "Authen");
            }

            return returnUrl;
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Register()
        {

            IEnumerable<SelectListItem> items = new List<SelectListItem>();
            items.ToList().Add(new SelectListItem { Text = "User", Value = Privilages.User.ToString()});
            items.ToList().Add(new SelectListItem { Text = "Supervisor", Value = Privilages.Supervisor.ToString() });
            items.ToList().Add(new SelectListItem { Text = "Admin", Value = Privilages.Admin.ToString() });
            List<string> p = new List<string>();
            p.Add(Privilages.User.ToString());
            p.Add(Privilages.Supervisor.ToString());
            p.Add(Privilages.Admin.ToString());
            ViewBag.plist = p;

            List<UserModel> pouzivatelia = new List<UserModel>();
            try
            {
                pouzivatelia = db.Accounts.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                pouzivatelia = null;
            }
            var us = User.Identity as ClaimsIdentity;
            if (us.IsAuthenticated)
            {
                ViewBag.Pr = us.FindFirst(ClaimTypes.Role).Value.ToString();
                if (ViewBag.Pr == "Root")
                {
                    items.ToList().Add(new SelectListItem { Text = "Root", Value = "9999" });
                }
            }

            ViewBag.PrvDropDown = items;
            ViewBag.Pouzivatelia = pouzivatelia;

            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            Privilages pv;
            if(model.Privlage == "User")
            {
                pv = Privilages.User;
            }else if(model.Privlage == "Supervisor")
            {
                pv = Privilages.Supervisor;
            }
            else if(model.Privlage == "Admin")
            {
                pv = Privilages.Admin;
            }
            else if(model.Privlage == "Root")
            {
                pv = Privilages.Root;
            }
            else
            {
                pv = Privilages.Device;
            }

            IEnumerable<SelectListItem> items = new List<SelectListItem>();
            items.ToList().Add(new SelectListItem { Text = "User", Value = Privilages.User.ToString() });
            items.ToList().Add(new SelectListItem { Text = "Supervisor", Value = Privilages.Supervisor.ToString() });
            items.ToList().Add(new SelectListItem { Text = "Admin", Value = Privilages.Admin.ToString() });
            List<string> p = new List<string>();
            p.Add(Privilages.User.ToString());
            p.Add(Privilages.Supervisor.ToString());
            p.Add(Privilages.Admin.ToString());
            ViewBag.plist = p;

            var us = User.Identity as ClaimsIdentity;
            if (us.IsAuthenticated)
            {
                ViewBag.Pr = us.FindFirst(ClaimTypes.Role).Value.ToString();
                if (ViewBag.Pr == "Root")
                {
                    items.ToList().Add(new SelectListItem { Text = "Root", Value = "9999" });
                }
            }

            var user = new AppUser
            {
                UserName = model.UserName,
                privilages = pv
            };

            var result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                using (AppDbContext db = new AppDbContext())
                {
                    if (pv== Privilages.User || pv == Privilages.Admin || pv == Privilages.Supervisor)
                    {
                        UserModel acc = new UserModel();
                        acc.Account = model.UserName;
                        acc.Name = model.Name;
                        acc.SurName = model.SurName;
                        acc.Privilage = pv;
                        acc.Password = model.Password;
                        acc.User = user.Id;
                        
                        db.Accounts.Add(acc);
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (SystemException e)
                        {
                            ModelState.AddModelError("", "User already existing");

                            return View();
                        }
                    }
                }
                await SignIn(user);
                
                return RedirectToAction("index", "settings", new { filter = "all", sft = "N" });
            }

               

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }

            return View();
        }

        [Authorize]
       public async Task<ActionResult> DeletUser(int? id)
        {
            UserModel userTodel = db.Accounts.Where(x => x.Id == id).First();
            var user = await userManager.FindByNameAsync(userTodel.Account);
            var logins = user.Logins;
            var rolesForUser = await userManager.GetRolesAsync(user.Id);
            var userclaims = await userManager.GetClaimsAsync(user.Id);
            using (var transaction = db.Database.BeginTransaction())
            {
                foreach(var log in logins.ToList())
                {
                    await userManager.RemoveLoginAsync(log.UserId, new UserLoginInfo(log.LoginProvider, log.ProviderKey));
                }
                if(rolesForUser.Count() > 0)
                {
                    foreach(var item in rolesForUser.ToList())
                    {
                        await userManager.RemoveFromRoleAsync(user.Id, " ");
                    }
                }
                if(userclaims.Count() > 0)
                {
                    foreach(var claim in userclaims.ToList())
                    {
                        await userManager.RemoveClaimAsync(user.Id, claim);
                    }
                }
                db.Users.Remove(user);
                transaction.Commit();
                transaction.Dispose();
                db.Accounts.Remove(userTodel);
                db.SaveChangesAsync();

            }
                return RedirectToAction("Register");
        }

        //Logout method
        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Authen");
        }
        //SignIn method
        private async Task SignIn(AppUser user)
        {
            var identity = await userManager.CreateIdentityAsync(
                user, DefaultAuthenticationTypes.ApplicationCookie);

            GetAuthenticationManager().SignIn(identity);
        }

        //Dispose sessions from users
        protected override void Dispose(bool disposing)
        {
            if (disposing && userManager != null)
            {
                userManager.Dispose();
            }
            base.Dispose(disposing);
        }

        private IAuthenticationManager GetAuthenticationManager()
        {
            var ctx = Request.GetOwinContext();
            return ctx.Authentication;
        }

        public ContentResult Send()
        {
            ContentResult result = new ContentResult();
            

            if(result == null)
            {
                RedirectToAction("Index", "Settings", new { filter = "all", sft = "N" });
            }

            return result;
        }
    }
}