﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PL;
using PL.Models;
using PL.Models.ViewModels;

namespace PL.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Products
        [HttpGet]
        public ActionResult Products()
        {
            List<ProductsModel> produkty = new List<ProductsModel>();
            List<ProjectModel> projekty = new List<ProjectModel>();
            try
            {
                produkty = db.Products.ToList();
                projekty = db.Projects.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                produkty = null;
                projekty = null;
            }
            List<string> items = new List<string>();
            foreach(ProjectModel projekt in projekty)
            {
                items.Add(projekt.Title);
            }
            ViewBag.plist = items;
            ViewBag.Produkty = produkty;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Products( ProductsViewModel productsModel)
        {
            if (productsModel.Product_Number != null && productsModel.ProjectID != null && productsModel.Name != null && productsModel.Time != null)
            {
                int index = db.Products.Count() + 1;
                ProductsModel newProduct = new ProductsModel();
                ProjectModel project = new ProjectModel();
                newProduct.Id = index;
                newProduct.Name = productsModel.Name;
                newProduct.Product_Number = productsModel.Product_Number;
                int time = productsModel.Time.Days * 24 + productsModel.Time.Hours;
                time = time * 60 + productsModel.Time.Minutes;
                newProduct.Time = TimeSpan.FromSeconds(time);
                newProduct.Description = productsModel.Description;
                
                List<ProjectModel> projects = db.Projects.ToList();
                foreach (ProjectModel p in projects)
                {
                    if (p.Title == productsModel.ProjectID)
                    {
                        newProduct.Project = p;
                        //p.Products.Add(newProduct);
                        project = p;
                        break;
                    }
                }
                //if (project != null) db.Entry(project).State = EntityState.Modified;

                db.Products.Add(newProduct);
                db.SaveChanges();
                return RedirectToAction("Products");
            }


            List<ProductsModel> produkty = new List<ProductsModel>();
            List<ProjectModel> projekty = new List<ProjectModel>();
            try
            {
                produkty = db.Products.ToList();
                projekty = db.Projects.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                produkty = null;
                projekty = null;
            }
            List<string> items = new List<string>();
            foreach (ProjectModel projekt in projekty)
            {
                items.Add(projekt.Title);
            }
            ViewBag.plist = items;
            ViewBag.Produkty = produkty;
            return View();
        }

     

     

        // GET: Products/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductsModel productsModel = db.Products.Find(id);
            if (productsModel == null)
            {
                return HttpNotFound();
            }
            List<ProjectModel> projekty = new List<ProjectModel>();
            try
            {
                projekty = db.Projects.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                projekty = null;
            }
            List<string> items = new List<string>();
            foreach (ProjectModel projekt in projekty)
            {
                items.Add(projekt.Title);
            }
            ViewBag.plist = items;
            return View(productsModel);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductsModel prodModel)
        {
            if (prodModel.Product_Number != null && prodModel.Project.Title != null && prodModel.Name != null && prodModel.Time != null)
            {
                ProductsModel newProduct = new ProductsModel();
                newProduct.Id = prodModel.Id;
                newProduct.Name = prodModel.Name;
                newProduct.Product_Number = prodModel.Product_Number;
                int time = prodModel.Time.Days * 24 + prodModel.Time.Hours;
                time = time * 60 + prodModel.Time.Minutes;
                newProduct.Time = TimeSpan.FromSeconds(time);
                newProduct.Description = prodModel.Description;
                newProduct.Project = prodModel.Project;
               
                db.Entry(newProduct).State = EntityState.Modified;
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }
                return RedirectToAction("Products");
            }
            else
            {
                return HttpNotFound();
            }
            return View(prodModel);
        }

        // GET: Products/Delete/5
        [HttpGet]
        public ActionResult Delete(int[] id)
        {
            if (id == null)
            {
                ModelState.AddModelError("", "Bad select ");
            }
            else
            {
                List<ProductsModel> pList = new List<ProductsModel>();
                for (int i = 0; i < id.Length; i++)
                {
                    ProductsModel productsModel = db.Products.Find(id[i]);
                    if (productsModel == null)
                    {
                        pList.Add(productsModel);
                    }
                }
                return View(pList);
            }
            return RedirectToAction("Products");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(List<ProductsModel> products)
        {
            if(products != null)
            {
                foreach(ProductsModel p in products)
                {
                    db.Products.Remove(p);
                }
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }
            }
            else
            {
                ModelState.AddModelError("", "Bad select");
            }
            return RedirectToAction("Products");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        int GetProjectId(string name)
        {
            List<ProjectModel> projects = db.Projects.ToList();
            int val = 0;
            foreach(ProjectModel pro in projects)
            {
                if(pro.Title == name)
                {
                    val = pro.Id;
                    break;
                }
            }
            return val;
        }
    }
}
