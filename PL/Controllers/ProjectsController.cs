﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PL;
using PL.Models;

namespace PL.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Projects
        public ActionResult Projects()
        {
            List<ProjectModel> projekty = new List<ProjectModel>();
            try
            {
                projekty = db.Projects.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                projekty = null;
            }

            ViewBag.Projekty = projekty;
            return View();
        }

        // GET: Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectModel projectModel = db.Projects.Find(id);
            if (projectModel == null)
            {
                return HttpNotFound();
            }
            return View(projectModel);
        }

        // GET: Projects/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Projects( ProjectModel projectModel)
        {
            if (projectModel.Title != null && projectModel.Project_Number != null && projectModel.Description != null)
            {
                db.Projects.Add(projectModel);
                db.SaveChanges();
                
            }
            else
            {
                ModelState.AddModelError("", "Project already exists!");
              
            }

            List<ProjectModel> projekty = new List<ProjectModel>();
            try
            {
                projekty = db.Projects.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                projekty = null;
            }

            ViewBag.Projekty = projekty;
            return View();
        }


        // POST: Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( ProjectModel projectModel)
        {
            if (projectModel.Title != null && projectModel.Project_Number != null && projectModel.Description != null)
            {
                db.Projects.Add(projectModel);
                db.SaveChanges();
                return RedirectToAction("Projects");
            }

            return View(projectModel);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectModel projectModel = db.Projects.Find(id);
            if (projectModel == null)
            {
                return HttpNotFound();
            }
            return View(projectModel);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectModel projectModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(projectModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Projects");
            }
            return View(projectModel);
        }

        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectModel projectModel = db.Projects.Find(id);
            if (projectModel == null)
            {
                return HttpNotFound();
            }
            else
            {
                db.Projects.Remove(projectModel);
                db.SaveChanges();
            }
            return RedirectToAction("Projects");
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
