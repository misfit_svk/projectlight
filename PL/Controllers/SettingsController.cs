﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PL.Models.Data;
using PL.Models.ViewModels;
using System.Security.Claims;
using PL.Models;
using System.Data.Entity;

namespace PL.Controllers
{
    public class SettingsController : Controller
    {
        AppDbContext db = new AppDbContext();
        
        public SettingsController()
        {
            try
            {
                if (User.Identity.IsAuthenticated != null)
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        ViewBag.Privlage = (User.Identity as ClaimsIdentity).FindFirst(ClaimTypes.Role).Value;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error:'{0}'", ex.Message);
                ViewBag.Privlage = "0";
            }
        }
        // GET: Settings
        [AllowAnonymous]
        
        public ActionResult Index(string filter, string sft)
        {
            if (filter == null) filter = "all";
            if (sft == null) sft = "N";
            List<SettingsViewModel> settingsViewList = new List<SettingsViewModel>();
            List<SettingsViewModel> setViewList = new List<SettingsViewModel>();
            List<SettingsModel> setList = new List<SettingsModel>();
            try
            {
                setList = db.Settings.ToList();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                settingsViewList = null;
            }
            
            foreach(SettingsModel s in setList)
            {
                SettingsViewModel newModel = new SettingsViewModel();
                newModel.SettingId = s.Id;
                newModel.Line = s.Line.Title;
                newModel.Line_id = s.Line.Id;
                newModel.Shift = s.Shift;
                newModel.State = s.Line.state.ToString();
                newModel.Efficiency = s.Efficiency.ToString() + "%";
                newModel.LedTime = s.Line.LedTime;
                newModel.Time = s.Line.Time;
                newModel.Product = s.Product.Name;
                newModel.Project = s.Product.Project.Title;
                newModel.Workers = s.Workers;

                settingsViewList.Add(newModel);
            }
            if(setList != null)
            {
                if(filter == "On")
                {
                   foreach(SettingsViewModel m in settingsViewList)
                    {
                        if(m.State == STATUS.ON.ToString())
                        {
                            if (sft == "N")
                            {
                                setViewList.Add(m);
                            }
                            else if (sft == "A")
                            {
                                if (m.Shift == "Shift A") setViewList.Add(m);
                            }
                            else if (sft == "B")
                            {
                                if (m.Shift == "Shift B") setViewList.Add(m);
                            }

                        }
                    }
                }
                else if(filter == "Off")
                {
                    foreach (SettingsViewModel m in settingsViewList)
                    {
                        if (m.State == STATUS.OFF.ToString())
                        {
                            if (sft == "N" )
                            {
                                setViewList.Add(m);
                            }
                            else if (sft == "A")
                            {
                                if (m.Shift == "Shift A") setViewList.Add(m);
                            }
                            else if(sft == "B")
                            {
                                if (m.Shift == "Shift B") setViewList.Add(m);
                            }
                        }
                    }
                }
                else if(filter == "StnBy")
                {
                    foreach (SettingsViewModel m in settingsViewList)
                    {
                        if (m.State == STATUS.STAND_BY.ToString())
                        {
                            if (sft == "N")
                            {
                                setViewList.Add(m);
                            }
                            else if (sft == "A")
                            {
                                if (m.Shift == "Shift A") setViewList.Add(m);
                            }
                            else if (sft == "B")
                            {
                                if (m.Shift == "Shift B") setViewList.Add(m);
                            }
                        }
                    }
                }
                else
                {
                    if (sft == "N")
                    {
                        setViewList = settingsViewList;
                    }
                    else if (sft == "A")
                    {
                        setViewList = settingsViewList.Where(x => x.Shift == "Shift A").ToList();
                    }
                    else if (sft == "B")
                    {
                        setViewList = settingsViewList.Where(x => x.Shift == "Shift B").ToList();
                    }
                }
            }
            List<int> Selected = new List<int>();


            ViewBag.Filt = filter;
            ViewBag.Shift = sft;
            ViewBag.Selected = Selected;
            ViewBag.Index = 0;
            ViewBag.Settings = setViewList;
            return View();

        }

        [AllowAnonymous]
        public ActionResult StartLine(int? id , string f , string s)
        {
            if (id != null)
            {
                LineModel line = db.Lines.Find(id);
                LineModel newLine = new LineModel();
                newLine.Id = line.Id;
                newLine.Device = line.Device;
                newLine.Enabled = line.Enabled;
                newLine.LedTime = line.LedTime;
                newLine.Set_state = STATUS.ON;
                newLine.state = line.state;
                newLine.Time = line.Time;
                newLine.Title = line.Title;
                line.state = STATUS.STAND_BY;
                line.Set_state = STATUS.ON;
                db.Entry(line).State = EntityState.Modified;
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }
                
            }
            return RedirectToAction("Index", new { filter = f, sft = s });
        }

        [AllowAnonymous]
        public ActionResult StopLine(int? id, string f, string s)
        {
            LineModel line = db.Lines.Find(id);
            LineModel newLine = new LineModel();
            newLine.Id = line.Id;
            newLine.Device = line.Device;
            newLine.Enabled = line.Enabled;
            newLine.LedTime = line.LedTime;
            newLine.Set_state = STATUS.OFF;
            newLine.state = line.state;
            newLine.Time = line.Time;
            newLine.Title = line.Title;
            line.state = STATUS.OFF;
            line.Set_state = STATUS.OFF;
            db.Entry(line).State = EntityState.Modified;
            try { db.SaveChanges(); }
            catch (Exception ex)
            {
                Console.WriteLine("Error:'{0}'", ex.Message);
            }
            return RedirectToAction("Index", new { filter = f, sft = s });
        }

        private TimeSpan GetTimeOfLine(ProjectModel project)
        {
            TimeSpan time = new TimeSpan();
            List<ProductsModel> Products = db.Products.Where(x => x.Project.Id == project.Id).ToList();
            foreach(ProductsModel product in Products)
            {
                time = time + product.Time;
            }
            


            return time;
        }

        [Authorize(Roles = "User, Supervisor, Admin, Root")]
        [HttpGet]
        public ActionResult AddSettings()
        {
            List<string> items = new List<string>();
            List<ProjectModel> Projects = db.Projects.ToList();
            if (Projects.Count != 0)
            {
                foreach (ProjectModel model in Projects)
                {

                    items.Add(model.Title.ToString());
                }
            }
            ViewBag.ProjectDropdown = items;
            return View();
        }

        [Authorize(Roles = "User, Supervisor, Admin, Root")]
        [HttpPost]
        public ActionResult AddSettings(SettingsViewModel model)
        {
            return View();
        }

        public bool CheckChBoxes()
        {
            if(ViewBag.Selected.Count() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


       [HttpGet]
        [AllowAnonymous]
        public ActionResult ManageLines()
        {
            List<SettingsViewModel> settingsViewList;
            var m = new SettingsViewModel();
            try
            {
                settingsViewList = db.Settings.ToArray().OrderBy(x => x.Line.Id).Select(x => new SettingsViewModel(x)).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                settingsViewList = null;
            }

            List<ProductsModel> produkty = new List<ProductsModel>();
            List<LineModel> linky = new List<LineModel>();
            try
            {
                produkty = db.Products.ToList();
                linky = db.Lines.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                produkty = null;
                linky = null;
            }
            List<string> items1 = new List<string>();
            foreach (ProductsModel produkt in produkty)
            {
                items1.Add(produkt.Name);
            }
            List<string> items2 = new List<string>(); 
            foreach(LineModel line in linky)
            {
                items2.Add(line.Title);
            }
            List<string> items3 = new List<string>();
            items3.Add("Shift A");
            items3.Add("Shift B");

            ViewBag.lines = items2;
            ViewBag.prod = items1;
            ViewBag.shift = items3;

            ViewBag.Settings = settingsViewList;
            if (settingsViewList == null) return View(m);
            

            List<int> Selected = new List<int>();

           
            ViewBag.Selected = Selected;
            ViewBag.Index = 0;
            
            return View(m);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageLines(SettingsViewModel settingModel)
        {
            if (settingModel.Efficiency == null) settingModel.Efficiency = "100";
            else if (settingModel.Efficiency == "0") settingModel.Efficiency = "1";
            
            if(settingModel.Line != null && settingModel.Workers > 0 && settingModel.Product != null  && settingModel.LedTime != null)
            {
                SettingsModel newSettingModel = new SettingsModel();
                int settingsId = db.Settings.Count() + 1;
                newSettingModel.Id = settingsId;
                newSettingModel.Shift = settingModel.Shift ;
                newSettingModel.Workers = settingModel.Workers;
                newSettingModel.Efficiency = Convert.ToInt32(settingModel.Efficiency);
                List<ProductsModel> prodkty = db.Products.Where(x => x.Name == settingModel.Product).ToList();
                List<LineModel> SelectedLinky = db.Lines.Where(x => x.Title == settingModel.Line).ToList() ;

                ProductsModel prod = prodkty.FirstOrDefault();
                LineModel linka = SelectedLinky.First();
                int time = prod.Time.Minutes * 60;
                time = time + prod.Time.Seconds;
                time = (int)((float)(time / newSettingModel.Workers) * (float) (2 - newSettingModel.Efficiency/100.0)); 
                linka.Time= TimeSpan.FromSeconds(time);
                time = settingModel.LedTime.Days * 24 + settingModel.LedTime.Hours;
                time = time * 60 + settingModel.LedTime.Minutes;
                linka.LedTime = TimeSpan.FromSeconds(time);
                LineModel newline = new LineModel();
                
                newline.Id = linka.Id;
                newline.Device = linka.Device;
                newline.Title = linka.Title;
                newline.Time = linka.Time;
                newline.LedTime = linka.LedTime;
                newline.state = STATUS.OFF;
                newline.Set_state = STATUS.OFF;
                newline.Enabled = true;
                try {
                    if (TryValidateModel(newline)){

                        db.Entry(linka).State = System.Data.Entity.EntityState.Modified;
                        try { db.SaveChanges(); }
                        catch (Exception ex) { Console.WriteLine("Error:'{0}'", ex.Message); }
                    }
                    db.Entry(linka).State = System.Data.Entity.EntityState.Modified;
                    try { db.SaveChanges(); }
                    catch (Exception ex) { Console.WriteLine("Error:'{0}'", ex.Message); }
                }
                catch (Exception ex) { Console.WriteLine("Error:'{0}'", ex.Message); }
                newSettingModel.Line = linka;
                 
                newSettingModel.Product = prod;
               
                
                db.Settings.Add(newSettingModel);

                try { db.SaveChanges(); }
                catch (Exception ex) { Console.WriteLine("Error:'{0}'", ex.Message); }
                 

            }
            else
            {
                if (settingModel.Workers <= 0) {
                    ModelState.AddModelError("", "Set up at least one worker to work!");
                   }
                else
                {
                    ModelState.AddModelError("", "Fill all entries for new settings!");
                }

            }
            List<SettingsViewModel> settingsViewList = new List<SettingsViewModel>();
            var m = new SettingsViewModel();
            try
            {
                settingsViewList = db.Settings.ToArray().OrderBy(x => x.Line.Id).Select(x => new SettingsViewModel(x)).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                settingsViewList = null;
            }

            List<ProductsModel> produkty = new List<ProductsModel>();
            List<LineModel> linky = new List<LineModel>();
            try
            {
                produkty = db.Products.ToList();
                linky = db.Lines.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: '{0}'", ex.Message);
                produkty = null;
                linky = null;
            }
            List<string> items1 = new List<string>();
            foreach (ProductsModel produkt in produkty)
            {
                items1.Add(produkt.Name);
            }
            List<string> items2 = new List<string>();
            foreach (LineModel line in linky)
            {
                items2.Add(line.Title);
            }
            List<string> items3 = new List<string>();
            items3.Add("Shift A");
            items3.Add("Shift B");

            ViewBag.lines = items2;
            ViewBag.prod = items1;
            ViewBag.shift = items3;

            ViewBag.Settings = settingsViewList;
            if (settingsViewList == null) return View(m);


            List<int> Selected = new List<int>();


            ViewBag.Selected = Selected;
            ViewBag.Index = 0;

            return View(m);
        }

    }
}