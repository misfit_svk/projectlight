﻿using Microsoft.AspNet.Identity;
using PL.Models;
using PL.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    

    public class DevicesController : Controller
    {
        
         
        private readonly UserManager<AppUser> userManager;
        AppDbContext db = new AppDbContext();
        // GET: Devices
        [HttpGet]
        public ActionResult Devices()
        {
            List<DeviceModel> devices = db.Devices.ToList();


            ViewBag.Devices = devices;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Devices( DeviceModel newDevice)
        {
            if(newDevice.Id != null && newDevice.Title != null && newDevice.Mask != null)
            {
                int id = db.Devices.Count() + 1;
                newDevice.Id = id;
                newDevice.STATUS = false;
                db.Devices.Add(newDevice);
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }
                for (int i = 1; i <5; i++)
                {
                    AddLine(newDevice);
                }
                
            }
            else
            {
                ModelState.AddModelError("", "Device alrady exist!");
            }

            return RedirectToAction("Devices");

        }

        public async Task<ContentResult> GetDatetime()
        {
            DateTime Today = DateTime.Now;
            string time = Today.Hour.ToString()+":"+Today.Minute.ToString()+":"+Today.Second.ToString()+" "+Today.Day.ToString()+"."+Today.Month.ToString()+"."+Today.Year.ToString();
            return Content(time);
        }


        public ActionResult DeleteDevice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceModel device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            else
            {

                DelLine(device);
                db.Devices.Remove(device);
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }
            }
            return RedirectToAction("Devices");
        }

        public  async Task<ContentResult> GetPreset(string mac,string o1_min,string o1_sec,string o1_status,string hold1,
                                                                          string o2_min, string o2_sec, string o2_status, string hold2, 
                                                                          string o3_min, string o3_sec, string o3_status,string hold3,
                                                                          string o4_min, string o4_sec, string o4_status, string hold4)
        {
            DeviceModel device = db.Devices.Where(x => x.Mask == mac).First();
            string Respons = "SetParse(" ;
            
            if (device != null)
            {
                List<LineModel> deviceLines = db.Lines.Where(x => x.Device.Id == device.Id).ToList();
                int i = 1;

                foreach (LineModel line in deviceLines)
                {
                    LineModel updateLine = new LineModel();

                    updateLine = line;
                   
                    switch (i)
                    {
                        case 1:
                            ImputModel imput = new ImputModel();
                            imput = GetInput(line, o1_min, o1_sec, o1_status, hold1);
                            Respons = Respons + imput.min + "," + imput.sec + "," + imput.status + "," + imput.time;
                            updateLine = imput.updateLine;
                            
                            break;
                        case 2:
                            ImputModel imput1 = new ImputModel();
                            imput1 = GetInput(line, o1_min, o1_sec, o1_status, hold1);
                            Respons = Respons + imput1.min + "," + imput1.sec + "," + imput1.status + "," + imput1.time;
                            updateLine = imput1.updateLine;
                            break;

                        case 3:
                            ImputModel imp = new ImputModel();
                            imp = GetInput(line, o1_min, o1_sec, o1_status, hold1);
                            Respons = Respons + imp.min + "," + imp.sec + "," + imp.status + "," + imp.time;
                            updateLine = imp.updateLine;
                            break;
                        case 4:
                            ImputModel imp1 = new ImputModel();
                            imp1 = GetInput(line, o1_min, o1_sec, o1_status, hold1);
                            Respons = Respons + imp1.min + "," + imp1.sec + "," + imp1.status + "," + imp1.time;
                            updateLine = imp1.updateLine;
                            break;        

                    }
                    i++;
                    if (i >= 5)
                    {
                        break;
                    }
                }
                Respons = Respons + ")";
                return Content(Respons);
            }
            else
            {
                Respons = Respons + "Bad adress)";
                return Content(Respons);
            }
        }

        public void AddLine(DeviceModel device)
        {
            int Id = db.Lines.Count();
            List<LineModel> lines = db.Lines.Where(x => x.Device.Id == device.Id).ToList();
            var c = lines.Count + 1;
            if (c < 5)
            {
                LineModel line = new LineModel();
                line.Id = Id + 1;
                line.Device = device;
                line.Title = device.Title + "-" + "Line" + c.ToString();
                line.Time = TimeSpan.FromSeconds(60);
                line.LedTime = TimeSpan.FromSeconds(3);
                line.state = STATUS.OFF;
                line.Set_state = STATUS.OFF;
                line.Enabled = false;
                
                db.Lines.Add(line);
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }

            }

        }
        
        public ImputModel GetInput(LineModel line, string o_min, string o_sec, string o_status, string hold)
        {
            ImputModel imp = new ImputModel();
            imp.updateLine = line;
            
            if (line.Set_state == STATUS.OFF)
            {
                imp.updateLine.state = STATUS.OFF; //Je
                imp.status = "0"; //Odosle
                imp.min = null;
                imp.sec = null;
            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.OFF && o_status == "0")
            {
                imp.updateLine.state = STATUS.STAND_BY;
                imp.status = "2";
                //cas
                if (line.Time == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Seconds == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes <= 0 && line.Time.Seconds <= 0)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else
                {
                    if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() == o_sec)
                    {
                        imp.min = null;
                        imp.sec = null;
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = null;
                        imp.sec = line.Time.Seconds.ToString();
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = null;
                    }
                    else
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = line.Time.Seconds.ToString();
                    }
                }
            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.OFF && o_status == "1")
            {
                imp.updateLine.state = STATUS.ON; //Je
                imp.status = "1";
                imp.min = null;
                imp.sec = null;

            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.OFF && o_status == "2")
            {
                imp.updateLine.state = STATUS.STAND_BY;
                imp.status = "2";
                //cas
                if (line.Time == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Seconds == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes <= 0 && line.Time.Seconds <= 0)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else
                {
                    if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() == o_sec)
                    {
                        imp.min = null;
                        imp.sec = null;
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = null;
                        imp.sec = line.Time.Seconds.ToString();
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = null;
                    }
                    else
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = line.Time.Seconds.ToString();
                    }
                }
            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.ON && o_status == "2")
            {
                imp.updateLine.state = STATUS.STAND_BY;
                imp.status = "2";
                //cas
            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.ON && o_status == "1")
            {
                imp.updateLine.state = STATUS.ON;
                imp.status = "1";
                imp.min = null;
                imp.sec = null;

            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.ON && o_status == "0")
            {
                imp.updateLine.state = STATUS.STAND_BY;
                imp.status = "2";
                //cas
                if (line.Time == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Seconds == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes <= 0 && line.Time.Seconds <= 0)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else
                {
                    if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() == o_sec)
                    {
                        imp.min = null;
                        imp.sec = null;
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = null;
                        imp.sec = line.Time.Seconds.ToString();
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = null;
                    }
                    else
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = line.Time.Seconds.ToString();
                    }
                }
            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.STAND_BY && o_status == "2")
            {
                imp.updateLine.state = STATUS.STAND_BY;
                imp.status = "2";
                //cas
                if (line.Time == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Seconds == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes <= 0 && line.Time.Seconds <= 0)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else
                {
                    if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() == o_sec)
                    {
                        imp.min = null;
                        imp.sec = null;
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = null;
                        imp.sec = line.Time.Seconds.ToString();
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = null;
                    }
                    else
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = line.Time.Seconds.ToString();
                    }
                }
            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.STAND_BY && o_status == "1")
            {
                imp.updateLine.state = STATUS.ON;
                imp.status = "1";
                imp.min = null;
                imp.sec = null;

            }
            else if (line.Set_state == STATUS.ON && line.state == STATUS.STAND_BY && o_status == "0")
            {
                imp.updateLine.state = STATUS.STAND_BY;
                imp.status = "2";
                //cas
                if (line.Time == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Seconds == null)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else if (line.Time.Minutes <= 0 && line.Time.Seconds <= 0)
                {
                    imp.min = o_min;
                    imp.sec = o_sec;
                }
                else
                {
                    if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() == o_sec)
                    {
                        imp.min = null;
                        imp.sec = null;
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = null;
                        imp.sec = line.Time.Seconds.ToString();
                    }
                    else if (line.Time.Minutes.ToString() == o_min && line.Time.Seconds.ToString() != o_sec)
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = null;
                    }
                    else
                    {
                        imp.min = line.Time.Minutes.ToString();
                        imp.sec = line.Time.Seconds.ToString();
                    }
                }
            }

            if (line.LedTime == null)
            {
                imp.time = hold;
            }
            else
            {
                string time = line.LedTime.Seconds.ToString() + ",";
                if (time == hold)
                {
                    imp.time = "null";
                }
                else
                {
                    imp.time = line.LedTime.Seconds.ToString();
                }
            }
            

            return imp;
        }

        public void DelLine(DeviceModel device)
        {
            List<LineModel> lines = db.Lines.Where(x => x.Device.Id == device.Id).ToList();
            if (lines != null)
            {
                foreach (LineModel line in lines)
                {
                    db.Lines.Remove(line);
                }
                try { db.SaveChanges(); }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:'{0}'", ex.Message);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}