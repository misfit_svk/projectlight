﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using PL.App_Start;
using PL.Controllers;

namespace PL
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static System.Timers.Timer timer = new System.Timers.Timer(300);
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFIlters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Hadler);
            timer.Enabled = true;
            
        }

        public void timer_Hadler(object sender, System.Timers.ElapsedEventArgs e)
        {
    
        }
        
    }
}
