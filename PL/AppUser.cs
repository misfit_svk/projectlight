﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using PL.Models;
using System.ComponentModel.DataAnnotations;

namespace PL
{
    public class AppUser: IdentityUser
    {
        [DataType(DataType.Text)]
        [Required]
        override public string UserName { get; set; }

        public Privilages privilages { get; set; }

    }

    public enum Privilages
    {
        User = 1,
        Supervisor = 50,
        Admin = 100,
        Device = 200,
        Root = 9999
    }
}