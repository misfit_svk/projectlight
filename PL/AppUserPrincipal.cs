﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using PL.Models.Data;

namespace PL
{
    public class AppUserPrincipal : ClaimsIdentity
    {
        public AppUserPrincipal(ClaimsPrincipal principal)
            : base(principal.Identity)
        {

        }

        public string UserName
        {
            get
            {
                return this.FindFirst(ClaimTypes.GivenName).Value;
            }
            
        }

        public int privilages
        {
            get
            {
                return Convert.ToInt16(FindFirst(ClaimTypes.Role).Value);

            }
        }
        

       
    }
}