﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PL.Models
{
    [Table("Settings")]
    public class SettingsModel
    {
        [Required]
        [Key]
        public int Id { get; set; }
       
        [Required]
        [Key]
        public  virtual LineModel Line { get; set; }

        public virtual ProductsModel Product { get; set; }

        [Required] 
        public int Workers { get; set; }

        [Required]
        public string Shift { get; set; }

        [Required]
        public float Efficiency { get; set; }
    }
}