﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using PL.Models.Data;

namespace PL.Models
{
    [Table("Lines")]
    public class LineModel
    {
        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        [Required]
        public virtual DeviceModel Device { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan LedTime { get; set; }

        [Required]
        public bool Enabled { get; set; }

        [Required]
        public STATUS state { get; set; }

        [Required]
        public STATUS Set_state { get; set; }
    }

    public enum STATUS
    {
        OFF =100, //0
        STAND_BY = 200,  //2
        ON = 300//1
    };
}