﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using PL.Models;

namespace PL.Models.Data
{
    [Table("Accounts")]
    public class UserModel
    {
        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Account { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string SurName { get; set; }

        [Required]
        public Privilages Privilage { get; set; }

        [Required]
        public string User { get; set; }


        
    }

    
}