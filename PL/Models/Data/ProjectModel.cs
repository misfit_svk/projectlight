﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PL.Models
{
    [Table("Projects")]
    public class ProjectModel
    {
        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Project_Number { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        public List<ProductsModel> Products { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }
    }
}