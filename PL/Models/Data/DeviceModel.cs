﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PL.Models.Data
{
    [Table("Devices")]
    public class DeviceModel
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        [Required]
        public string Mask { get; set; }

        [Required]
        public bool STATUS { get; set; }

    }

 
}