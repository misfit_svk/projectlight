﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using PL.Models.Data;

namespace PL.Models
{
    [Table("Logs")]
    public class LogsModel
    {
        [Required]
        [Key]
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Account { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public DateTime Time { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        public virtual UserModel User { get; set; }

    }
}