﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Models.Data
{
    public class ImputModel
    {
        public LineModel updateLine { get; set; }
        public string min { get; set; }
        public string sec { get; set; }
        public string status { get; set; }
        public  string time { get; set; }
    }
}