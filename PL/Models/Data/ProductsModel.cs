﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PL.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace PL.Models
{
    [Table("Products")]
    public class ProductsModel
    {
        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Product_Number { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        public virtual ProjectModel Project { get; set; }
    }
}