﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PL.Models;

namespace PL.Models.ViewModels
{
    public class ProductsViewModel
    {
        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Product_Number { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        public int minutes { get; set; }

        public int seconds { get; set; }

        public string ProjectID { get; set; }
    }
}