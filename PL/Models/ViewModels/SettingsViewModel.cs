﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PL.Models.Data;

namespace PL.Models.ViewModels
{
    public class SettingsViewModel
    {
        AppDbContext db = new AppDbContext();
        public SettingsViewModel()
        {

        }

        public SettingsViewModel(SettingsModel row)
        {
            SettingId = row.Id;
            Line = row.Line.Title;
            STATUS stav = row.Line.state;
            State = stav.ToString();

            Product = row.Product.Name;
            Project = row.Product.Project.Title;
            Time = row.Line.Time;
            LedTime = row.Line.LedTime;
            Workers = row.Workers;
            Efficiency = row.Efficiency.ToString();
            Line_id = row.Line.Id;
        }
        [Required]
        [Key]
        public int SettingId { get; set; }

        [Required]
        [Key]
        [Display(Name = "Line title")]
        public string Line { get; set; }

        [Display(Name = "Line state")]
        public string State { get; set; }

        [Required]
        [Display(Name = "Project title")]
        public string Project { get; set; }

        [Required]
        [Display(Name = "Product title")]
        public string Product { get; set; }


        [Required]
        [Display(Name = "Shift")]
        public string Shift { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:mm\\:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan Time { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:mm\\:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan LedTime { get; set; }

        [Required]
        [Display(Name = "Workers Count")]
        public int Workers { get; set; }

        [Required]
        public string Efficiency { get; set; }

        public int Line_id { get; set; }



        

    }
}