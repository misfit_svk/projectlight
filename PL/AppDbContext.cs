﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using PL.Models;
using PL.Models.Data;

namespace PL
{
    public class AppDbContext : IdentityDbContext<AppUser>
    {
        

        public AppDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserModel> Accounts { get; set; }

        public DbSet<DeviceModel> Devices { get; set; }
        public DbSet<SettingsModel> Settings { get; set; }

        public DbSet<LineModel> Lines { get; set; }

        public DbSet<ProjectModel> Projects { get; set; }

        public DbSet<ProductsModel> Products { get; set; }

        public DbSet<LogsModel> Logs { get; set; }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
    }
}